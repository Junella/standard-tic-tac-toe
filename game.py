def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()


def game_over(board, position):
    print("GAME OVER")
    print_board(board)
    print(position, "has won")
    exit()


def is_row_winner(board, row_position):
    # if all boxes in same row match
    # position, position + 1, position + 2
    pos = row_position
    if board[pos] == board[pos + 1] and board[pos] == board[pos + 2]:
        return True


def is_column_winner(board, column_position):
    # loop through positions
    # position, position + 3, position + 6
    pos = column_position
    if board[pos] == board[pos + 3] and board[pos] == board[pos + 6]:
        return True

    return


board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, 0):
        game_over(board, board[0])
    elif is_row_winner(board, 3):
        game_over(board, board[3])
    elif is_row_winner(board, 6):
        game_over(board, board[6])
    elif is_column_winner(board, 0):
        game_over(board, board[0])
    elif is_column_winner(board, 1):
        game_over(board, board[1])
    elif is_column_winner(board, 2):
        game_over(board, board[2])

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
